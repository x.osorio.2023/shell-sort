import sys


def order_items(songs: list, i: int, gap: int) -> list:
    cancion = songs[i][0]
    reproduciones = songs[i][1]
    j = i
    while j >= gap and songs[j - gap][1] < reproduciones:
        songs[j] = songs[j - gap]
        j -= gap
        songs[j] = (cancion, reproduciones)
    return songs


def sort_music(songs: list) -> list:
    total = len(songs)
    gap = total // 2
    for i in range(gap, total):
        songs = order_items(songs, i, gap)
    gap //= 2

    return songs


def create_dictionary(arguments: list) -> dict:
    songs: dict = {}
    pos = 0
    try:
        while pos < len(arguments):
            song_name = arguments[pos]
            pos += 1
            if pos < len(arguments):
                reproduciones = int(arguments[pos])
                songs[song_name] = reproduciones
            else:
                sys.exit(f"Error: '{arguments[pos]}' no es un número válido.")
            pos += 1
    except:
        sys.exit("Error: Se ha de proporcionar un número valido de reproducciones para la canción.")
    return songs


def main():
    args = sys.argv[1:]

    if len(args) % 2 != 0 or len(args) == 0:
        sys.exit("Error: Debes proporcionar pares de canción y número de reproducciones.")

    songs: dict = create_dictionary(args)

    sorted_songs: list = sort_music(list(songs.items()))

    sorted_dict: dict = dict(sorted_songs)
    print(sorted_dict)


if __name__ == '__main__':
    main()
